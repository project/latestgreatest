
General
-------

latestgreatest.module displays a block containing a number of entries from any news aggregator category. Users with voting permissions can flag any of these items as worthy of attention, and if enough users vote for an item in this fashion, the item will NOT scroll off the list when newer entries arrive.

Requirements
------------

Naturally, aggregator.module needs to be enabled for the module to do its work. votingapi.module is also necessary for the user-driven promotion features.

Installation
------------

First, make sure that VotingAPI has been installed and its mySql file has been run against your database. Then copy the latestgreatest directory to your /modules directory.

Go to the admin/modules page and enable aggregator, votingapi, and latestgreatest if they aren't already. Create an aggregator category for the feeds you'd like to display in your Latest and Greatest block. Then go to the admin/blocks page, and configue the Latest and Greatest block. Select the category you'd like to use for the block and configure the thresholds. On high-traffic sites, setting the voting threshold higher will ensure you only get the 'best of the best.'